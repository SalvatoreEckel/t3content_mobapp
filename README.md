# EXT:t3content_mobapp #

This extension provides flux content elements for the t3cms theme mobapp

### Abandoned extension ###

This extension has no maintainer at the moment. If you like to get the maintainer of this extension, fill out the TYPO3.org registration form: https://extensions.typo3.org/faq/get-maintainer/

### General Information ###

* t3content_mobapp
* v2.2.3

### How do I get set up? ###

* Install the extension from TER
* Make sure **EXT:t3themes_mobapp** is loaded
* Make sure **EXT:flux** is loaded

* Have fun with your new content elements.

### Who do I talk to? ###

* Salvatore Eckel
* salvaracer@gmx.de